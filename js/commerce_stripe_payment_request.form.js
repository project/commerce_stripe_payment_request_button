(function ($, Drupal, drupalSettings) {
    /**
     * Disable the continue buttons in the checkout process once they are clicked
     * and provide a notification to the user.
     */
    Drupal.behaviors.commerceStripeWebPaymentRequest = {
        attach: function (context) {
            /**
             * Detect what browser we're on.
             */

            var browser = (function (agent) {
                switch (true) {
                    case agent.indexOf("edge") > -1:
                        return "ie";
                    case agent.indexOf("msie") > -1:
                        return "ie";
                    case agent.indexOf("opr") > -1 && !!window.opr:
                        return "opera";
                    case agent.indexOf("chrome") > -1 && !!window.chrome:
                        return "chrome";
                    case agent.indexOf("trident") > -1:
                        return "ie";
                    case agent.indexOf("firefox") > -1:
                        return "firefox";
                    case agent.indexOf("safari") > -1:
                        return "safari";
                    default:
                        return "other";
                }
            })(window.navigator.userAgent.toLowerCase());

            var $form = $(drupalSettings.stripe_payment_request.form_id);
            // Check if Stripe Payment Request tab active.

            // Create our Payment Request button.
            var stripe = Stripe(drupalSettings.stripe_payment_request.publishable_key);
            var paymentRequest = stripe.paymentRequest(drupalSettings.stripe_payment_request.paymentRequest);

            $form.once('stripe-payment-request-setup-wrapper').each(function () {
                var elements = stripe.elements();
                var prButton = elements.create('paymentRequestButton', {
                    paymentRequest: paymentRequest,
                    style: {
                        paymentRequestButton: {
                            height: '44px',
                            theme: 'light-outline',
                            type: 'buy',
                        },
                    },
                });

                // Check if we can make payments.
                (async () => {
                    const result = await paymentRequest.canMakePayment();
                    // If we can make Stripe Apple/Google payments, show the button.
                    // Only NULL means you can't use it, return with applePay true/false is only meant for apple pay
                    if (result) {
                        prButton.mount('#payment-request-button');
                        //Drupal.behaviors.commerceStripeWebPaymentRequest.showButton();
                    }
                    // Else, if they can't make payments, show the setup info.
                    else {
                        document.getElementById('payment-request-button').style.display = 'none';
                        //Drupal.behaviors.commerceStripeWebPaymentRequest.showSetUpButton();
                    }
                })();

                // Send paymentMethod to server
                paymentRequest.on('paymentmethod', function (ev) {
                    var payment_intent_data = {
                        payment_method_remote_id: ev.paymentMethod.id,
                        result: ev,
                        payment_request: drupalSettings.stripe_payment_request.paymentRequest,
                    };

                    var payment_intent_path = drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + drupalSettings.stripe_payment_request.payment_intent_path;

                    fetch(payment_intent_path, {
                        method: 'POST',
                        body: JSON.stringify({
                            data: payment_intent_data
                        }),
                        headers: {
                            'content-type': 'application/json'
                        },
                    })
                        .then(function (response) {
                            response.json().then(function (json) {
                                if (json.error) {
                                    Drupal.commerceStripe.displayError(result.error.message);
                                    return false;
                                } else if (json.requires_action) {
                                    ev.complete('success');
                                    handleAction(json, payment_intent_path, ev);
                                } else {
                                    ev.complete('success');
                                    window.location.href = json.redirect_uri;
                                }

                            });
                        });
                });


                function handleAction(response, payment_intent_path, ev) {
                    var data = {
                        payment_method: response.payment_method_remote_id
                    };

                    stripe.handleCardPayment(response.payment_intent_client_secret, data).then(function (result) {
                        if (result.error) {
                            Drupal.commerceStripe.displayError(result.error.message)
                        } else {
                            //We need to send back info to controller and to add new data to order and payment
                            fetch(payment_intent_path, {
                                method: 'POST',
                                body: JSON.stringify({
                                    data: result
                                }),
                                headers: {
                                    'content-type': 'application/json'
                                },
                            })
                                .then(function (response) {
                                    response.json().then(function (json) {
                                        if (json.error) {
                                            Drupal.commerceStripe.displayError(result.error.message);
                                            return false;
                                        } else if (json.requires_action) {
                                            ev.complete('success');
                                            handleAction(json, payment_intent_path, ev);
                                        } else {
                                            ev.complete('success');
                                            window.location.href = json.redirect_uri;
                                        }

                                    });
                                });
                        }
                    });


                }

                paymentRequest.on('shippingaddresschange', function (ev) {
                    // Perform server-side request to fetch shipping options
                    var shipping_path = drupalSettings.path.baseUrl + drupalSettings.path.pathPrefix + drupalSettings.stripe_payment_request.shipping_path;
                    fetch(shipping_path, {
                        method: 'POST',
                        body: JSON.stringify({
                            shippingAddress: ev.shippingAddress,
                        }),
                        headers: {
                            'content-type': 'application/json'
                        },
                    }).then(function (response) {
                        return response.json();
                    }).then(function (result) {
                        if (result.shippingOptions && result.shippingOptions.length) {
                            ev.updateWith({
                                status: 'success',
                                shippingOptions: result.shippingOptions,
                            });

                        } else {
                            ev.updateWith({
                                status: 'invalid_shipping_address'
                            });
                        }
                    });
                });
            });
        },
    };

    Drupal.commerceStripeButton = {
        displayError: function (errorMessage) {
            $('#payment-errors').html(Drupal.theme('commerceStripeButtonError', errorMessage));
        }
    }
    Drupal.theme.commerceStripeButtonError = function (message) {
        return $('<div class="messages messages--error"></div>').html(message);
    }
})(jQuery, Drupal, drupalSettings);
