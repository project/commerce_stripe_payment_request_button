INTRODUCTION
-------------
This modules outlines provides integration with STRIPE libraries to enable usage of Stripe Payment Request button
https://stripe.com/docs/stripe-js/elements/payment-request-button
which in turn gives  your customers possibility of using Apple Pay, Google Pay, Microsoft Pay

REQUIREMENTS
------------
Stripe PHP library and JS stripe elements library, Stripe PHP is included via composer, stripe elements JS is loaded remotly

INSTALLATION
-------------
Enable module, go into admin/commerce/config/payment-gateways and enable already added "Commerce Stripe - payment request button"
payment.
If you are collecting shipping info, you need to install shipping module and configure it per https://git.drupalcode.org/project/commerce_shipping/blob/8.x-2.x/README.md
If you want to collect phone number, you need to have fild_phone in your profile entity and enable this feature in payment gateway config

CONFIGURATION
-------------
Once you enable payment gateway as described above, add publishable and secret key


DECOUPLED (HEADLESS) SUPPORTED
------------------------------
This module now supports also decoupled (headless) checkout via Stripe SDK.

If you have a complex multi currency setup , then it is also possible to set the exact config in the order state.
The key name the module expects is "stripe_payment_request_button_config_id";

NOTES
----------------------------
----------------------------
 This module will by default install its own payment gateway with machine name "commerce_stripe_payment_request_button" and put it into disabled mode.
 If you need multiple currency instances of this payment gateway you can create them, but the name has to be "commerce_stripe_payment_request_button_$currency"
 where $currency is the iso 4217 currency code (for example "commerce_stripe_payment_request_button_eur" or "commerce_stripe_payment_request_button_usd" ).

 Reason is that this machine name is used across code(loaded configurations etc) and if it is changed, then the code will not work.

- This modules installs custom checkout flow, as there is no need for standard flow.
- Uses default order type, which in turns selects which shipment type is used.
- For shipping method, it uses the one user selects from pop up, you need to have at least one that is available for this payment gateway




