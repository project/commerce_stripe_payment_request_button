<?php

namespace Drupal\commerce_stripe_payment_request_button\Services;

use Drupal;
use Exception;
use Stripe\Stripe;
use Drupal\commerce_stripe_payment_request_button\Services\PaymentRequestButtonHelperServiceInterface;
use Stripe\PaymentIntent;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\ShipmentItem;

use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;
use Drupal\profile\Entity\Profile;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;

use Drupal\commerce_price\Price;

/**
 * Class PaymentRequestButtonHelperService
 *
 * @package Drupal\commerce_stripe_payment_request_button\Service
 */
class PaymentRequestButtonHelperService implements PaymentRequestButtonHelperServiceInterface {
  /**
   * Check if the payment is Google Pay, Apple pay or Link
   *
   * @param PaymentIntent $paymentIntent
   * @return string $wallet
   */
  public function getWalletType(PaymentIntent $paymentIntent): string {
    $wallet = 'Link';

    if (isset($paymentIntent->charges->data[0])) {
      /**
       * @var \Stripe\PaymentIntent $paymentIntent
       */
      $charges = $paymentIntent->charges->data[0];
      $charges_values = $charges->jsonSerialize();
      $walletTypeSet = isset($charges_values['payment_method_details']['card']['wallet']['type']);
      if ($walletTypeSet == TRUE) {
        $walletType = $charges_values['payment_method_details']['card']['wallet']['type'];
        if (strpos($walletType, 'apple') !== false) {
          $wallet = 'Apple pay';
        }
        if (strpos($walletType, 'google') !== false) {
          $wallet = 'Google pay';
        }
      }
    }
    return $wallet;
  }

  /**
   * Get the order configuration by currency
   *
   * @param string $currency
   * @return class $data
   */
  public function getOrderPaymentConfiguration(OrderInterface $order, string $currency) {
    $configCurrency = $order->getData('stripe_payment_request_button_config_id');
    $configBase = 'commerce_stripe_payment_request_button';

    if (!$configCurrency) {
      $configCurrency = $configBase . '_' . $currency;
    }
    $configName = $configCurrency;

    // Load curency config
    $config = \Drupal::config('commerce_payment.commerce_payment_gateway.' . $configCurrency);

    if (!$config->get('status')) {
      // Load default config
      $config = \Drupal::config('commerce_payment.commerce_payment_gateway.' . $configBase);
      $configName = $configBase;
    }

    $data = new \stdClass();

    $data->config = $config->get('configuration');
    $data->configName = $configName;

    return $data;
  }

  /**
   * Update drupal payment from the payment intent
   *
   * @param OrderInterface $order
   * @param string $intentId
   * @return PaymentIntent $paymentIntent
   */
  public function updateDrupalPaymentByPaymentIntent(OrderInterface $order, string $intentId) {

    $paymentIntent = PaymentIntent::retrieve($intentId);
    $payments = \Drupal::entityQuery('commerce_payment')
      ->condition('order_id', $order->id())
      ->execute();

    $payment = Payment::load(reset($payments));
    $payment->setRemoteId($paymentIntent->id);
    $payment->save();

    // Reset this step as it gets to NULL on cart refresh
    $order->get('checkout_flow')->setValue("payment_request_button_flow");
    $order->get('checkout_step')->setValue("payment");
    $order->save();
    return  $paymentIntent;
  }

  /**
   * Set the following order data: checkout_flow,checkout_step,email,payment_gateway,stripe_intent,wallet
   *
   * @param OrderInterface $order
   * @param PaymentIntent $paymentIntent
   * @param string $email
   * @param string $configName
   * @param string $shipping
   * @return void
   */
  public function setOrderData(OrderInterface $order, PaymentIntent $paymentIntent, string $email, string $configName, string $shipping = ''): void {
    $order->get('checkout_flow')->setValue("payment_request_button_flow");
    $order->get('checkout_step')->setValue("payment");
    $order->setEmail($email);
    $order->get('payment_gateway')->setValue($configName);
    $order->setData('stripe_intent', $paymentIntent->id);
    $order->setData('wallet', $this->getWalletType($paymentIntent));
    if (!empty($shipping)) {
      $order->setData('payment_request_button_shipping', $shipping);
    }
    $order->save();
  }


  /**
   * Create a drupal payment from the data provided
   *
   * @param OrderInterface $order
   * @param PaymentGatewayInterface $paymentGateway
   * @param PaymentIntent $paymentIntent
   * @param string $currentUser
   * @return Payment $payment
   */
  public function createDrupalPayment(OrderInterface $order, PaymentGatewayInterface $paymentGateway, PaymentIntent $paymentIntent, string $currentUser) {

    $paymentMethod = $this->createPaymentMethod($order, $paymentGateway, $paymentIntent->payment_method);
    $payment = Payment::create([
      'state' => 'new',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $paymentGateway->id(),
      'order_id' => $order->id(),
      'remote_id' => $paymentIntent->id,
      'payment_gateway_mode' => $paymentGateway->getPlugin()->getMode(),
      'payment_method' => $order->get('payment_method'),
      'expires' => 0,
      'uid' => $currentUser,
    ]);
    $payment->save();


    return $payment;
  }


  /**
   * Check if the shipping profile exists or create a new one.
   * Afterwards return the profile for processing
   *
   * @param $order
   * @param $paymentIntent
   * @param $requestPhone
   * @param $payerPhone
   *
   * @return mixed|null
   */
  public function getOrderShippingProfile($order, $paymentIntent, $requestPhone, $payerPhone) {
    // We only get the full name from Apple/Google pay, so we need to "guess" the given
    // name and the family name.
    $shippingInfo = [];
    $profiles = $order->collectProfiles();
    $billingDetails = $paymentIntent->charges->data[0]->billing_details;
    $shippingDetails = $paymentIntent->shipping;
    if (!is_null($paymentIntent->shipping) && count($paymentIntent->shipping->address)) {
      $shippingInfo = $paymentIntent->shipping->address;
    } elseif (count($paymentIntent->charges['data'][0])) {
      $shippingInfo = $billingDetails->address;
    }

    if (is_array($shippingInfo)) {
      $shippingInfo = json_decode(json_encode($shippingInfo));
    }
    if (!isset($shippingInfo->line1) && isset($shippingInfo->addressLine[0])) {
      $shippingInfo->line1 = $shippingInfo->addressLine[0];
    }
    if (!isset($shippingInfo->line2) && isset($shippingInfo->addressLine[1])) {
      $shippingInfo->line2 = $shippingInfo->addressLine[1];
    }

    $names = [];
    $copyNames = false;

    //if there are no shipping details that means that We have to use the billing name
    if (is_null($shippingDetails)) {
      $copyNames = true;
    } elseif (isset($shippingDetails->name) && strlen(trim($shippingDetails->name) >= 1)) {
      $names = explode(' ', (string) trim($shippingDetails->name));
      if (empty($names[0]) || empty($names[1])) {
        $copyNames = true;
      }
    }
    if ($copyNames == true && strlen(trim($billingDetails->name)) >= 1) {
      $names = explode(' ', (string) trim($billingDetails->name));
    } elseif ($copyNames == true) {
      $billingProfileAddress = $order->getBillingProfile()->address->first();
      $names = [
        !is_null($billingProfileAddress->getGivenName()) ? $billingProfileAddress->getGivenName() : '',
        !is_null($billingProfileAddress->getFamilyName()) ? $billingProfileAddress->getFamilyName() : '',
      ];
    }
    if (isset($shippingInfo->postalCode)) {
      $shippingInfo->postal_code = $shippingInfo->postalCode;
    }

    $given_name = array_shift($names);
    $family_name = implode(' ', $names);
    $shippingProfile = NULL;

    //if the user is authenticated
    if ($order->getCustomerId() != 0) {
      //load all profiles to see if shipping address already exists to prevent database bloat
      $searchData = [
        'type' => 'customer_shipping',
        'uid' => $order->getCustomerId(),
        'address' => [
          'country_code' => $shippingInfo->country,
          'locality' => $shippingInfo->city,
          'postal_code' => $shippingInfo->postal_code,
          'address_line1' => $shippingInfo->line1,
          'given_name' => $given_name,
          'familiy_name' => $family_name
        ]
      ];
      if (isset($shippingInfo->region)) {
        $searchData['address']['administrative_area'] = $shippingInfo->region;
      }
      if (isset($profiles['shipping'])) {
        $profile = $profiles['shipping'];
      } else {
        $profiles = \Drupal::entityTypeManager()
          ->getStorage('profile')->loadByProperties($searchData);
        $profile = reset($profiles);
      }
      //check if the address exists
      if (!is_bool($profile)) {
        if ($profile->hasField('field_phone') && $requestPhone) {
          $profile->set('field_phone', $payerPhone);
        }
        $profile->save();
        $shippingProfile = $profile;
      }
    }
    $orderShipments = $order->shipments->referencedEntities();

    $firstShipment = reset($orderShipments);

    //if profile exists set it as the order shipping profile else create new order
    if (!is_null($shippingProfile)) {
      if ($shippingProfile->hasField('field_phone') && $requestPhone) {
        $shippingProfile->set('field_phone', $payerPhone);
      }
      $shippingProfile->save();
      $firstShipment->setShippingProfile($shippingProfile);
    } else {
      $createData =  [
        'type' => 'customer_shipping',
      ];
      if ($order->getCustomerId() > 0) {
        $createData['uid'] = $order->getCustomerId();
      }

      $shippingProfile = Profile::create($createData);
      $shippingProfile->address->given_name = $given_name;
      $shippingProfile->address->family_name = $family_name;
      $shippingProfile->address->country_code = $shippingInfo->country;
      $shippingProfile->address->locality = $shippingInfo->city;
      $shippingProfile->address->administrative_area = $shippingInfo->state;
      $shippingProfile->address->postal_code = $shippingInfo->postal_code;
      $shippingProfile->address->address_line1 = $shippingInfo->line1;
      if (isset($shippingInfo->line2)) {
        $shippingProfile->address->address_line2 = $shippingInfo->line2;
      }
      if (isset($shippingInfo->region)) {
        $shippingProfile->address->address_administrative_area = $shippingInfo->region;
      }

      if ($shippingProfile->hasField('field_phone') && $requestPhone) {
        $shippingProfile->set('field_phone', $payerPhone);
      }
      $shippingProfile->save();
    }

    return $shippingProfile;
  }

  public function handleShipping($order, $paymentIntent, $requestPhone = FALSE) {
    $shipment = null;
    $charge = $paymentIntent->charges->data[0];
    if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
      $shipment = $order->get('shipments')->entity;
    } else {
      $shipment = Shipment::create([
        'type' => 'default',
        'order_id' => $order->id(),
        'title' => 'Shipment',
        'state' => 'ready',
      ]);
    }

    // Loop through order items and add them to shipment
    foreach ($order->getItems() as $order_item) {
      $quantity = $order_item->getQuantity();
      $purchased_entity = $order_item->getPurchasedEntity();

      //TODO Skip weight if there is none, currently breaks order flow
      if ($purchased_entity->get('weight')->isEmpty()) {
        $weight = new Weight(1, WeightUnit::GRAM);
      } else {
        $weight_item = $purchased_entity->get('weight')->first();
        $weight = $weight_item->toMeasurement();
      }

      $shipment_item = new ShipmentItem([
        'order_item_id' => $order_item->id(),
        'title' => $purchased_entity->label(),
        'quantity' => $quantity,
        'weight' => $weight->multiply($quantity),
        'declared_value' => $order_item->getTotalPrice(),
      ]);
      $shipment->addItem($shipment_item);
    }

    $shipping_method = $order->get('shipments')->entity->getShippingMethod();

    if (!is_null($order->getData('payment_request_button_shipping'))) {
      $shipping_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');
      $shipping_method = $shipping_method_storage->load($order->getData('payment_request_button_shipping'));
    } elseif (isset($values['result']['shippingOption']) &&  $shipping_method->id() != $values['data']['result']['shippingOption']['id']) {
      $shipping_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');
      $shipping_method = $shipping_method_storage->load($values['data']['result']['shippingOption']['id']);
    }
    $shipment->setTitle($shipping_method->getName());
    $shipment->setShippingMethodId($shipping_method->id());
    $shipment->setShippingMethod($shipping_method);
    $shipment->save();
    $order->set('shipments', $shipment);

    $order->save();

    // Add proper shipping method
    $shipping_method_plugin = $shipping_method->getPlugin();
    $shipping_rates = $shipping_method_plugin->calculateRates($shipment);
    $shipping_rate = reset($shipping_rates);
    $shipping_service = $shipping_rate->getService()->getId();
    $shipping_method_amount = $shipping_rate->getAmount();

    //if the order currency is different from the default currency, check the if there is an correct shipping field set
    if (
      $shipping_method_amount->getCurrencyCode() != $order->getTotalPrice()
        ->getCurrencyCode()
      && isset($shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
          ->getCurrencyCode()])
    ) {
      $newAmount = $shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
        ->getCurrencyCode()];
      if (is_numeric($newAmount['number']) && $newAmount['number'] > 0) {
        $shipping_method_amount = new Price($newAmount['number'], $newAmount['currency_code']);
      }
    }
    //amount is not zero and there are not currency fields appended We use the commerce_currency_resolver to convert the amount
    elseif (
      $shipping_method_amount->getNumber() > 0 &&
      $shipping_method_amount->getCurrencyCode() != $order->getTotalPrice()
        ->getCurrencyCode()
      && isset($shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
          ->getCurrencyCode()])
    ) {
      $shipping_method_amount = Drupal::service('commerce_currency_resolver.calculator')
        ->priceConversion($shipping_method_amount, $order->getTotalPrice()
          ->getCurrencyCode());
    }

    $shipment->setAmount($shipping_method_amount);

    $phone = '';
    if (is_array($charge) && isset($charge['data']['result']['shippingAddress']['phone'])) {
      $phone = $charge['data']['result']['shippingAddress']['phone'];
    } elseif (!is_array($charge)) { //just to check that it is an object
      $phone = $charge->billing_details->phone;
    }
    $profile = $this->getOrderShippingProfile($order, $paymentIntent, $requestPhone, $phone);

    $shipment->setShippingProfile($profile);
    $shipment->setShippingService($shipping_service);
    $shipment->save();

    $order->set('shipments', $shipment);
    $order->save();
  }

  public function processProfile(OrderInterface $order, $paymentIntent, $requestPhone = true) {
    // Deal with billing profile
    $profile = $order->getBillingProfile();
    $currentUser = $order->getCustomerId();
    $stripeBilling = $paymentIntent->charges['data'][0]->billing_details;
    $stripeShipping = $paymentIntent->shipping;
    if (empty($profile)) {
      $profile = Profile::create([
        'type' => 'customer',
        'uid' => $currentUser,
      ]);
      $profile->save();
    }

    $current_name = $stripeBilling->name;
    if (strlen($current_name < 2)) {
      $current_name = $stripeShipping->name;
    }
    $names = explode(' ', (string) $current_name);
    $given_name = array_shift($names);
    $family_name = implode(' ', $names);

    $profile->address->given_name = $given_name;
    $profile->address->family_name = $family_name;
    $profile->address->country_code = $stripeBilling->address->country;
    $profile->address->locality = $stripeBilling->address->city;
    $profile->address->administrative_area = $stripeBilling->address->state;
    $profile->address->postal_code = $stripeBilling->address->postal_code;
    $profile->address->address_line1 = $stripeBilling->address->line1;
    if (isset($stripeBilling->address->line2)) {
      $profile->address->address_line2 = $stripeBilling->address->line2;
    }
    if ($profile->hasField('field_phone') && $requestPhone) {
      $profile->set('field_phone', $stripeBilling->phone);
    }
    $profile->save();

    $order->setBillingProfile($profile);
    $order->save();
  }

  /**
   * @{inheritdoc}
   */
  public function createPaymentMethod(OrderInterface $order, PaymentGatewayInterface $payment_gateway, string $paymentMethod) {
    $billing_profile = $order->getBillingProfile();
    if ($billing_profile instanceof ProfileInterface) {
      $type = 'stripe_payment_request_button';

      /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
      $payment_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment_method');
      $payment_method = $payment_method_storage->create([
        'type' => $type,
        'payment_gateway' => $payment_gateway->id(),
        'remote_id' => $paymentMethod,
        'uid' => $order->getCustomerId(),
        'billing_profile' => $order->getBillingProfile()
      ]);
      $order->set('payment_method', $payment_method)->save();
      return $payment_method;
    }

    return [
      'type' => 'errors',
      'errors' => [
        [
          'status' => '422',
          'title' => 'Payment Method Error',
          'detail' => 'Billing profile does not exist on cart order!'
        ]
      ]
    ];
  }
}
