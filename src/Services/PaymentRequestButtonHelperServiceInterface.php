<?php

namespace Drupal\commerce_stripe_payment_request_button\Services;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Stripe\Stripe;
use Stripe\PaymentIntent;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;


/**
 * Interface PaymentRequestButtonHelperServiceInterface
 *
 * @package Drupal\commerce_stripe_payment_request_button\Service
 */
interface PaymentRequestButtonHelperServiceInterface {
  /**
   * Check if the payment is Google Pay, Apple pay or Link
   *
   * @param PaymentIntent $paymentIntent
   * @return string $wallet
   */
  public function getWalletType(PaymentIntent $paymentIntent);

  /**
   * Get the order configuration by order data or currency
   *
   * @param OrderInterface $order
   * @param string $currency
   * @return class $data
   */
  public function getOrderPaymentConfiguration(OrderInterface $order, string $currency);

  /**
   * Update drupal payment from the payment intent
   *
   * @param OrderInterface $order
   * @param string $intentId
   * @return PaymentIntent $paymentIntent
   */
  public function updateDrupalPaymentByPaymentIntent(OrderInterface $order, string $intentId);

  /**
   * Set the following order data: checkout_flow,checkout_step,email,payment_gateway,stripe_intent,wallet
   *
   * @param OrderInterface $order
   * @param PaymentIntent $paymentIntent
   * @param string $email
   * @param string $configName
   * @param string $shipping
   * @return void
   */
  public function setOrderData(OrderInterface $order, PaymentIntent $paymentIntent, string $email, string $configName, string $shipping = '');

  /**
   * Create a drupal payment from the data provided
   *
   * @param OrderInterface $order
   * @param PaymentGateway $paymentGateway
   * @param PaymentIntent $paymentIntent
   * @param string $currentUser
   * @return Payment $payment
   */
  public function createDrupalPayment(OrderInterface $order, PaymentGatewayInterface $paymentGateway, PaymentIntent $paymentIntent, string $currentUser);

  /**
   * Check if the shipping profile exists or create a new one.
   * Afterwards return the profile for processing
   *
   * @param $order
   * @param $shipping_info
   * @param $requestPhone
   * @param $payerPhone
   *
   * @return mixed|null
   */
  public function getOrderShippingProfile($order, $shipping_info, $requestPhone, $payerPhone);

  public function handleShipping($order, $paymentIntent, $requestPhone = FALSE);

  public function processProfile(OrderInterface $order, $paymentIntent, $requestPhone = true);


  /**
   * Create paymentMethod for the order.
   *
   * @param OrderInterface $order
   * @param PaymentGatewayInterface $payment_gateway
   * @param string $paymentMethod (Stripe payment intent payment method)
   * @return void
   */
  public function createPaymentMethod(OrderInterface $order, PaymentGatewayInterface $payment_gateway, string $paymentMethod);
}
