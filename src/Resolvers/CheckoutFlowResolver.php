<?php

namespace Drupal\commerce_stripe_payment_request_button\Resolvers;

use Drupal\commerce_checkout\Resolver\CheckoutFlowResolverInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class CheckoutFlowResolver implements CheckoutFlowResolverInterface {

  /**
   * The checkout flow storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $checkoutFlowStorage;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->checkoutFlowStorage = $entity_type_manager->getStorage('commerce_checkout_flow');
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderInterface $order) {
    if (!empty($order->get('payment_gateway')->first())) {
      $payment_gateway_item = $order->get('payment_gateway')->first()->getValue();
      $payment_gateway_id = $payment_gateway_item['target_id'];
      if (strpos($payment_gateway_id, 'commerce_stripe_payment_request_button') !== false) {
        return $this->checkoutFlowStorage->load('commerce_stripe_payment_request_button');
      }
    }
  }

}
