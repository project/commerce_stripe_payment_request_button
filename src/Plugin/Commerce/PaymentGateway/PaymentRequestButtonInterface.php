<?php

namespace Drupal\commerce_stripe_payment_request_button\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides the interface for the Stripe payment gateway.
 */
interface PaymentRequestButtonInterface extends OffsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Get the Stripe API Publisable key set for the payment gateway.
   *
   * @return string
   *   The Stripe API publishable key.
   */
  public function getPublishableKey();

  /**
   * If data exists in the payment gateway data, unset it after cancellation or completetion
   *
   * @param OrderInterface $order
   * @return void
   */
  public function unsetPaymentData(OrderInterface $order);
}
