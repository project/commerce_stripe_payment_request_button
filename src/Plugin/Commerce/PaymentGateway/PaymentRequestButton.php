<?php

namespace Drupal\commerce_stripe_payment_request_button\Plugin\Commerce\PaymentGateway;

use Stripe\Stripe;
use Stripe\Balance;
use Stripe\Error\Base;
use Stripe\PaymentIntent;
use Stripe\Refund;


use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe\ErrorHelper;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_stripe_payment_request_button\Services\PaymentRequestButtonHelperServiceInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\commerce_payment\Entity\Payment;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides Stripe Payment Request Button offsite payment.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_stripe_payment_request_button",
 *   label = @Translation("Stripe Payment Request Button"),
 *   display_label = @Translation("Stripe Payment Request Button"),
 *   payment_method_types = {"stripe_payment_request_button"},
 *   credit_card_types = {
 *     "amex", "discover", "mastercard", "visa",
 *   },
 *   js_library = "commerce_stripe_payment_request_button/form",
 * )
 */
class PaymentRequestButton extends OffsitePaymentGatewayBase implements PaymentRequestButtonInterface {

  /**
   * Stripe payment request button helper service.
   *
   * @var \Drupal\commerce_stripe_payment_request_button\Services\PaymentRequestButtonHelperServiceInterface
   */
  protected $paymentRequestButtonService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    Stripe::setApiKey($this->configuration['secret_key']);
    $this->paymentRequestButtonService = \Drupal::service('commerce_stripe_payment_request_button.helper');
  }

  /**
   * Sets the API key after the plugin is unserialized.
   */
  public function __wakeup() {
    Stripe::setApiKey($this->configuration['secret_key']);
  }

  /**
   * {@inheritdoc}
   */
  public function getPublishableKey() {
    return $this->configuration['publishable_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'publishable_key' => '',
        'secret_key' => '',
        'stripe_account_id' => '',
        'api_logging' => '',
        'request_shipping' => FALSE,
        'request_phone' => FALSE,
        'capture_method' => 'automatic',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['publishable_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publishable Key'),
      '#default_value' => $this->configuration['publishable_key'],
      '#required' => TRUE,
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];
    $form['stripe_account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stripe Account ID'),
      '#description' => t('You can find your Stripe Account ID <a href="https://dashboard.stripe.com/settings/account" target="_blank">here</a>'),
      '#default_value' => $this->configuration['stripe_account_id'],
      '#required' => TRUE,
    ];

    $form['request_shipping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable fetching shipping data from Apple/Google Pay, disable this only if you have digital goods that dont need shipping address'),
      '#default_value' => $this->configuration['request_shipping'],
      '#required' => FALSE,
    ];

    $form['request_phone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable fetching phone number data from Apple/Google Pay'),
      '#default_value' => $this->configuration['request_phone'],
      '#required' => FALSE,
    ];

    $form['api_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log webhook messages onNotify for debugging'),
      '#default_value' => $this->configuration['api_logging'],
      '#required' => FALSE,
    ];

    $form['capture_method'] = [
      '#type' => 'select',
      '#title' => t('Capturing method'),
      '#options' => array("automatic" => "Automatic", "manual" => "Manual"),
      '#default_value' =>  $this->configuration['capture_method'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      // Validate the secret key.
      $expected_livemode = $values['mode'] == 'live' ? TRUE : FALSE;
      if (!empty($values['secret_key'])) {
        try {
          Stripe::setApiKey($values['secret_key']);
          // Make sure we use the right mode for the secret keys.
          if (Balance::retrieve()->offsetGet('livemode') != $expected_livemode) {
            $form_state->setError($form['secret_key'], $this->t('The provided secret key is not for the selected mode (@mode).', ['@mode' => $values['mode']]));
          }
        } catch (Base $e) {
          $form_state->setError($form['secret_key'], $this->t('Invalid secret key.'));
        }
      }
      // TODO, check if any items are shipable
      // Check if shipping module is installed
      if ($values['request_shipping'] != 0) {
        if (!\Drupal::moduleHandler()->moduleExists('commerce_shipping')) {
          $form_state->setError($form['request_shipping'], $this->t("Commerce shipping module is not enabled, you can't use \"Enable fetching shipping data from Apple/Google Pay...\""));
        } else {
          $storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');

          $query = $storage->getAggregateQuery();
          $query->count();
          $count = $query->execute();

          if ($count == 0) {
            $link = Link::fromTextAndUrl('Shipping methods', Url::fromRoute('entity.commerce_shipping_method.collection'));
            $form_state->setError($form['request_shipping'], $this->t("You need to have at least one shipping method at %link to enable \"Enable fetching shipping data from Apple/Google Pay...\" ", ['%link' => $link->toString()]));
          }
        }
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['publishable_key'] = $values['publishable_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['stripe_account_id'] = $values['stripe_account_id'];
      $this->configuration['api_logging'] = $values['api_logging'];
      $this->configuration['request_shipping'] = $values['request_shipping'];
      $this->configuration['request_phone'] = $values['request_phone'];
      $this->configuration['capture_method'] = $values['capture_method'];
    }
  }


  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    try {
      $remote_id = $payment->getRemoteId();
      $intent = PaymentIntent::retrieve($remote_id);
      $amount_to_capture = $this->toMinorUnits($amount);
      if ($intent) {
        if ($intent->status != 'requires_capture') {
          throw new PaymentGatewayException('Only requires_capture PaymentIntents can be captured.');
        }
        $intent->capture(['amount_to_capture' => $amount_to_capture]);
      }
    } catch (Base $e) {
      ErrorHelper::handleException($e->getMessage());
    }

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Void Stripe payment - release uncaptured payment.
    try {
      $remote_id = $payment->getRemoteId();
      $intent = PaymentIntent::retrieve($remote_id);

      if ($intent) {
        $statuses_to_void = [
          'requires_payment_method',
          'requires_capture',
          'requires_confirmation',
          'requires_action',
        ];
        if (!in_array($intent->status, $statuses_to_void)) {
          throw new PaymentGatewayException('The PaymentIntent cannot be voided.');
        }
        $intent->cancel();
      } else {
        $data = [
          'payment_intent' => $remote_id,
        ];
        // Voiding an authorized payment is done by creating a refund.
        $release_refund = Refund::create($data);
        ErrorHelper::handleErrors($release_refund);
      }
    } catch (Base $e) {
      ErrorHelper::handleException($e->getMessage());
    }

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      $remote_id = $payment->getRemoteId();
      $minor_units_amount = $this->toMinorUnits($amount);
      $data = ['amount' => $minor_units_amount];

      if (strpos($remote_id, "pi_") === 0) {
        $data['payment_intent'] = $remote_id;
      }
      else {
        $data['charge'] = $remote_id;
      }

      $refund = Refund::create($data, ['idempotency_key' => \Drupal::getContainer()->get('uuid')->generate()]);
      ErrorHelper::handleErrors($refund);
    } catch (Base $e) {
      ErrorHelper::handleException($e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    } else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }


  /**
   * {@inheritdoc}
   */
  public function processOrder(OrderInterface $order) {
    if (!$order->isLocked()) {
      $order->lock()->save();
    }

    //get the current currency order
    $paymentIntentId = $order->getData('stripe_intent');
    $currency = strtolower($order->getTotalPrice()->getCurrencyCode());
    $orderPaymentConfiguration = $this->paymentRequestButtonService->getOrderPaymentConfiguration($order, $currency);

    // Init Stripe and config
    $configName = $orderPaymentConfiguration->configName;
    $config = $orderPaymentConfiguration->config;

    Stripe::setApiKey($config['secret_key']);

    $paymentIntent = PaymentIntent::retrieve($paymentIntentId);

    if (empty($order) || $order->getState()->value == 'canceled') {
      $order->unlock()->save();
      $response = [
        'redirect_uri' => Url::fromRoute(Url::fromRoute('commerce_cart.page')
          ->toString()),
      ];
      return new JsonResponse($response);
    }

    if ($order->id() != $paymentIntent->metadata->order_id) {
      $order->unlock()->save();
      $response = [
        'success' => FALSE,
        'error' => $this->t('Order id from the PaymentIntent does not match our records')
      ];
      return new JsonResponse($response);
    }

    if ($order->id() == $paymentIntent->metadata->order_id) {

      $payments = \Drupal::entityQuery('commerce_payment')
        ->condition('order_id', $order->id())
        ->execute();

      if (count($payments)) {
        $payment = Payment::load(reset($payments)); //cannot flip
        $payment->setRemoteId($paymentIntent->id);
        $payment->save();
      } elseif ($order->hasItems()) {
        // Put order in proper status and assign values
        $this->paymentRequestButtonService->setOrderData($order, $paymentIntent, $paymentIntent->metadata->email, $configName);

        // Create payment for this order and give it values
        $payment_gateway = $this->entityTypeManager
          ->getStorage('commerce_payment_gateway')
          ->load($configName);

        $payment = $this->paymentRequestButtonService->createDrupalPayment($order, $payment_gateway, $paymentIntent, $order->getCustomerId());

        if ($config['capture_method'] == "manual") {
          $payment->setState("authorization");
        }
        $payment->save();
      }
      $this->paymentRequestButtonService->processProfile($order, $paymentIntent, $config['request_phone']);

      // Deal with shipping stuff only if we selected that we want to fetch shipping address
      if ($config['request_shipping']) {
        $this->paymentRequestButtonService->handleShipping($order, $paymentIntent, $config['request_phone']);
      }

      if ($paymentIntent->status == PaymentIntent::STATUS_SUCCEEDED) {
        if (!$payment->isCompleted()) {
          $workflow = $payment->getState()->getWorkflow();
          //authorization passed already on frontend
          $transition = $workflow->getTransition('authorize');
          $payment->getState()->applyTransition($transition);

          //funds are already passed already on frontend
          $transition = $workflow->getTransition('capture');
          $payment->getState()->applyTransition($transition);
          $payment->save();
        }

        // If We used the set data then after completion we need to remove it
        $this->unsetPaymentData($order);

        $response = [
          'success' => TRUE,
          'redirect_uri' => Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])
              ->toString() . '/payment',
        ];
      }
    }
    return new JsonResponse($response);
  }

  /**
   * {@inheritDoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {

    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));

    // If We used the set data then after cancellation we need to remove it
    $this->unsetPaymentData($order);

    if ($order->isLocked()) {
      $order->unlock()->save();
    }

    $response = [
      'redirect_uri' => Url::fromRoute(Url::fromRoute('commerce_cart.page')
        ->toString()),
    ];
    return new JsonResponse($response);
  }

  public function unsetPaymentData(OrderInterface $order) {
    //unset the payment intent. If the order is completed it will be stored within the payment
    $order->unsetData('stripe_intent');

    $orderDataConfig = $order->getData('stripe_payment_request_button_config_id');
    if ($orderDataConfig) {
      $order->unsetData('stripe_payment_request_button_config_id');
    }
    //save order
    $order->save();
  }
}
