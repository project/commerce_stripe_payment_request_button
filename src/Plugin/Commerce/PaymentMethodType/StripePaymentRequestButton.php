<?php

namespace Drupal\commerce_stripe_payment_request_button\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the ideal payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "stripe_payment_request_button",
 *   label = @Translation("Google pay/Apple pay/Microsoft pay"),
 *   create_label = @Translation("Google pay/Apple pay/Microsoft pay"),
 * )
 */
class StripePaymentRequestButton extends PaymentMethodTypeBase {

    /**
     * {@inheritdoc}
     */
    public function buildLabel(PaymentMethodInterface $payment_method) {
        return $this->pluginDefinition['label'];
    }
}
