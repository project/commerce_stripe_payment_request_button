<?php

namespace Drupal\commerce_stripe_payment_request_button\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowBase;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;

/**
 * Provides the default multistep checkout flow.
 *
 * @CommerceCheckoutFlow(
 *   id = "payment_request_button_flow",
 *   label = "Commerce Stripe payment request button Flow",
 * )
 */
class PaymentRequestButtonFlow extends CheckoutFlowWithPanesBase {


  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    // Each checkout flow plugin defines its own steps.
    // These two steps are always expected to be present.
    return [
      'payment' => [
        'label' => $this->t('Payment'),
        'next_label' => $this->t('Pay and complete purchase'),
        'has_sidebar' => FALSE,
        'hidden' => TRUE,
      ],
      'complete' => [
        'label' => $this->t('Complete'),
        'next_label' => $this->t('Complete checkout'),
        'has_sidebar' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPanes() {
    $panes = parent::getPanes();
    // Create a blacklist of panes we disallow adding to steps.
    $black_list = [
      'contact_information',
      'completion_register',
      'login',
      'payment_information',
      'payment_process',
    ];
    return array_diff_key($panes, array_combine($black_list, $black_list));
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $step_id = NULL) {
    $form = parent::buildForm($form, $form_state, $step_id);

    switch ($step_id) {
      case 'payment':
        try {
          // Load payment(s) for this order
          // TODO check if below is proper way to do this, maybe we should load by specific payment
          // gateway and filter by status, find only NEW and not failed transactions
          $payment_gateway = $this->order->get('payment_gateway')->entity;
          $payment_gateway_plugin = $payment_gateway->getPlugin();
          if (strpos($payment_gateway_plugin->pluginId, 'commerce_stripe_payment_request_button') !== FALSE) {
            $payment_storage = \Drupal::entityTypeManager()
              ->getStorage('commerce_payment');
            $payments = $payment_storage->loadMultipleByOrder($this->order);
            $payments = array_filter($payments, function ($payment) use ($payment_gateway) {
              return $payment->getPaymentGatewayId() == $payment_gateway->id();
            });
            $payment = reset($payments);
            if ($payment->getState()->getId() == "new") {
              $payment->setState('completed');
              $payment->save();
            }
            else {
              if ($payment->getState()->getId() == "authorization") {
                // You need to manually capture this payment, you can do it with UI or some automatatic scripting
              }
            }
          }
          $this->redirectToStep('complete');
        } catch (DeclineException $e) {
          \Drupal::messenger()->addError($e->getMessage());
          throw new NeedsRedirectException(Url::fromRoute('commerce_cart.page')
            ->toString());
        } catch (PaymentGatewayException $e) {
          \Drupal::messenger()->addError($e->getMessage());

          $this->order->get('checkout_flow')->setValue(NULL);
          $this->order->get('checkout_step')->setValue(NULL);
          $this->order->unlock();
          $this->order->save();
          throw new NeedsRedirectException(Url::fromRoute('commerce_cart.page')
            ->toString());
        }
        break;

      case 'complete':
        $form['completion_message']['#theme'] = 'commerce_checkout_completion_message';
        $form['completion_message']['#order_entity'] = $this->order;
        break;
    }

    return $form;
  }

}
