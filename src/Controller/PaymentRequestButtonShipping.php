<?php

namespace Drupal\commerce_stripe_payment_request_button\Controller;

use Drupal;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_store\Entity\Store;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Locale\CountryManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PaymentRequestButtonShipping {

  public function handleShipping(EntityInterface $commerce_order, Request $request) {
    // Transform JSON values to array
    $values = Json::decode($request->getContent());
    $order = $commerce_order;

    if ($order->hasItems()) {
      //if country not blocked
      if (
        isset($this->getBlockedShippingCountries($order->getStore()
            ->id())[0]) &&
        $this->getBlockedShippingCountries($order->getStore()
          ->id())[0] == 'none' ||
        (isset($values['shippingAddress']['country']) && !in_array($values['shippingAddress']['country'], $this->getBlockedShippingCountries($order->getStore()
            ->id())))
      ) {
        // Check if we have some shipments made from before and delete them
        if ($order->hasField('shipments') && !$order->get('shipments')
            ->isEmpty()) {
          // Delete shipment made on previous selection
            if ($order->get('shipments')->first()->entity != null){
              $order->get('shipments')->first()->entity->delete();
            }
          unset($order->shipments);
        }
        if (
          !$order->hasField('shipments') || $order->get('shipments')
            ->isEmpty()
        ) {
          $first_shipment = Shipment::create([
            'type' => 'default',
            'order_id' => $order->id(),
            'title' => 'Shipment',
            'state' => 'ready',
          ]);

          // Find rate, we use the first one, pretending there is only one we want to use, fixed rate for order
          $shipping_method_storage = Drupal::entityTypeManager()
            ->getStorage('commerce_shipping_method');
          $shipping_methods = $shipping_method_storage->loadMultipleForShipment($first_shipment);

          // Loop through shipping methods and add them to array so it can be sent as Response to Stripe
          foreach ($shipping_methods as $shipping_method) {
            $first_shipment->setShippingMethod($shipping_method);
            $shipping_method_plugin = $shipping_method->getPlugin();
            $shipping_rates = $shipping_method_plugin->calculateRates($first_shipment);
            $shipping_rate = reset($shipping_rates);
            $shipping_method_amount = $shipping_rate->getAmount();

            //if the order currency is different from the default currency, check the if there is an correct shipping field set
            if (
              $shipping_method_amount->getCurrencyCode() != $order->getTotalPrice()
                ->getCurrencyCode()
              && isset($shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
                  ->getCurrencyCode()])
            ) {
              $newAmount = $shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
                ->getCurrencyCode()];
              if (is_numeric($newAmount['number']) && $newAmount['number'] > 0) {
                $shipping_method_amount = new Price($newAmount['number'], $newAmount['currency_code']);
              }
            }
            //amount is not zero and there are not currency fields appended We use the commerce_currency_resolver to convert the amount
            elseif (
              $shipping_method_amount->getNumber() > 0 &&
              $shipping_method_amount->getCurrencyCode() != $order->getTotalPrice()
                ->getCurrencyCode()
              && isset($shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
                  ->getCurrencyCode()])
            ) {
              $shipping_method_amount = Drupal::service('commerce_currency_resolver.calculator')
                ->priceConversion($shipping_method_amount, $order->getTotalPrice()
                  ->getCurrencyCode());
            }

            $first_shipment->setAmount($shipping_method_amount);
            $response_shipping_methods[] = [
              'id' => $shipping_method->id(),
              'label' => $shipping_method->getName(),
              'detail' => $shipping_rate->getService()->getLabel(),
              'amount' => $this->toMinorUnits($shipping_method_amount),
            ];
          }
        }
      }
    }

    $response['shippingOptions'] = $response_shipping_methods;
    return new JsonResponse($response);
  }

  /**
   * get blocked shippied countries
   *
   * @param int $storeId
   *
   * @return array $blockedCountries
   */
  private
  function getBlockedShippingCountries($storeId): array {
    $blockedCountries = [];
    if (count($this->getAvailableShippingCountries($storeId)) == 1 && $this->getAvailableShippingCountries($storeId)[0] == 'all') {
      $blockedCountries[] = 'none';
    }
    else {
      $blockedCountries = array_diff(array_keys(CountryManager::getStandardList()), $this->getAvailableShippingCountries($storeId));
    }
    return $blockedCountries;
  }

  /**
   * get all available shipping countries
   *
   * @param int $storeId
   *
   * @return array $availableCountries
   */
  private
  function getAvailableShippingCountries($storeId): array {
    $store = Store::load($storeId);
    $availableCountries = [];
    if (
      $store->get('shipping_countries') && $store->get('shipping_countries')
        ->isEmpty()
    ) {
      $availableCountries[] = 'all';
    }
    elseif ($store->get('shipping_countries')) {
      foreach ($store->get('shipping_countries') as $country_item) {
        $availableCountries[] = $country_item->value;
      }
    }
    return $availableCountries;
  }

  /**
   * Converts the given amount to its minor units.
   *
   * For example, 9.99 USD becomes 999.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return int
   *   The amount in minor units, as an integer.
   */
  protected function toMinorUnits(Price $amount) {
    $currency_storage = Drupal::entityTypeManager()
      ->getStorage('commerce_currency');
    $currency = $currency_storage->load($amount->getCurrencyCode());
    $fraction_digits = $currency->getFractionDigits();
    $number = $amount->getNumber();
    if ($fraction_digits > 0) {
      $number = Calculator::multiply($number, pow(10, $fraction_digits));
    }

    return intval(round($number, 0));
  }

}
