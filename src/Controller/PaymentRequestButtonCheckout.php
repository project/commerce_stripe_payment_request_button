<?php

namespace Drupal\commerce_stripe_payment_request_button\Controller;

use Stripe\Stripe;
use Stripe\PaymentIntent;
use Stripe\Exception\ApiErrorException;

use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_stripe\ErrorHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;
use Drupal\profile\Entity\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Exception;

class PaymentRequestButtonCheckout extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $route_match;

  /**
   * Creates a new ViewModeAccessCheck.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    RouteMatchInterface $route_match
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->routeMatchInterface = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('current_route_match')
    );
  }

  /**
   * Handles the incoming order from the "classic" checkout process
   *
   * @param EntityInterface $commerce_order
   * @param Request $request
   * @return JsonResponse
   */
  public function handleOrder(EntityInterface $commerce_order, Request $request) {
    // Transform JSON values to array
    $values = Json::decode($request->getContent());
    $shipping_info = '';
    if (isset($values["data"]["result"]["shippingOption"]["shippingAddress"])) { //if multiple shipping addresses are available
      $shipping_info = $values["data"]["result"]["shippingOption"]["shippingAddress"];
    } elseif (isset($values["data"]["result"]["shippingAddress"])) {
      $shipping_info = $values["data"]["result"]["shippingAddress"];
    }
    $order = $commerce_order;
    //get the current currency order
    $currency = strtolower($order->getTotalPrice()->getCurrencyCode());

    // Get the configuration of the payment gateway
    $orderPaymentConfiguration = $this->getOrderPaymentConfiguration($currency);

    $configName = $orderPaymentConfiguration->configName;
    $config = $orderPaymentConfiguration->config;
    Stripe::setApiKey($config['secret_key']);

    // If order has problems, take it back to cart
    if (empty($order) || $order->getState()->value == 'canceled') {
      $response = [
        'redirect_uri' => Url::fromRoute(Url::fromRoute('commerce_cart.page')
          ->toString()),
      ];
      return new JsonResponse($response);
    }

    // Check if we already have payment intent
    $intentId = $order->getData('stripe_intent');
    if ($intentId !== NULL) {
      try {
        $intent = $this->updateDrupalPaymentByPaymentIntent($order, $intentId);
      } catch (ApiErrorException $e) {
        ErrorHelper::handleException($e);
      }
    } else {
      try {
        $shippingPhone = $values["data"]['result']['payerPhone'];
        $shippingName = $values["data"]['result']['payerName'];
        if (
          isset($values["data"]['result']['shippingAddress']['recipient']) &&
          !empty($values["data"]['result']['shippingAddress']['recipient'])
        ) {
          $shippingName = $values["data"]['result']['shippingAddress']['recipient'];
        }
        if (
          isset($values["data"]['result']['shippingAddress']['phone']) &&
          !empty($values["data"]['result']['shippingAddress']['phone'])
        ) {
          $shippingPhone = $values["data"]['result']['shippingAddress']['phone'];
        }
        $paymentIntentData = [
          'payment_method' => $values["data"]["payment_method_remote_id"],
          'amount' => $values["data"]["payment_request"]["total"]["amount"],
          'currency' => $values["data"]["payment_request"]["currency"],
          'payment_method_types' => ['card'],
          'capture_method' => $config['capture_method'],
          'description' => 'Order ' . $order->id(),
          'metadata' => ['order_id' => $order->id()],
          'confirmation_method' => 'automatic',
          'confirm' => TRUE,
          'shipping' => [
            'name' => $shippingName,
            'phone' => $shippingPhone,
            'address' => [
              'line1' => $shipping_info['addressLine'][0],
              'line2' => isset($shipping_info['addressLine'][1]) ? $shipping_info['addressLine'][1] : '',
              'city' => $shipping_info['city'],
              'state' => isset($shipping_info['region']) ? $shipping_info['region'] : '',
              'postal_code' => $shipping_info['postalCode'],
              'country' => $shipping_info['country'],
            ],
          ]

        ];
        // payment_request_button_shipping
        // Create payment intent
        $intent = PaymentIntent::create($paymentIntentData);

        if ($order->hasItems()) {
          // Put order in proper status and assign value
          $shipping = '';
          if (isset($values['data']['result']['shippingOption']['id'])) {
            $shipping = $values['data']['result']['shippingOption']['id'];
          }
          $this->setOrderData($order, $intent, $values["data"]["result"]["payerEmail"], $configName, $shipping);

          // Create payment for this order and give it values
          $payment_gateway = $this->entityTypeManager
            ->getStorage('commerce_payment_gateway')
            ->load($configName);

          $current_user = $order->getCustomerId();

          $payment = $this->createDrupalPayment($order, $payment_gateway, $intent, $current_user);

          if ($config['capture_method'] == "manual") {
            $payment->setState("authorization");
          }
          $payment->save();

          // Deal with billing profile
          $profile = $order->getBillingProfile();

          if (empty($profile)) {
            $profile = Profile::create([
              'type' => 'customer',
              'uid' => $current_user,
            ]);
            $profile->save();
          }

          $current_name = $values["data"]["result"]["paymentMethod"]["billing_details"]["name"];
          if (isset($values["data"]["result"]["payerName"])) {
            $current_name = $values["data"]["result"]["payerName"];
          }
          $names = explode(' ', $current_name);
          $given_name = array_shift($names);
          $family_name = implode(' ', $names);

          $profile->address->given_name = $given_name;
          $profile->address->family_name = $family_name;
          $profile->address->country_code = $values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["country"];
          $profile->address->locality = $values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["city"];
          $profile->address->administrative_area = $values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["state"];
          $profile->address->postal_code = $values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["postal_code"];
          $profile->address->address_line1 = $values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["line1"];
          if (isset($values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["line2"])) {
            $profile->address->address_line2 = $values["data"]["result"]["paymentMethod"]["billing_details"]["address"]["line2"];
          }
          if ($profile->hasField('field_phone') && $config['request_phone']) {
            $profile->set('field_phone', $values["data"]["result"]["paymentMethod"]["billing_details"]["phone"]);
          }
          $profile->save();

          $order->setBillingProfile($profile);
          $order->save();

          // Deal with shipping stuff only if we selected that we want to fetch shipping address
          if ($config['request_shipping'] && !empty($shipping_info)) {
            $this->handleShipping($order, $shipping_info, $values, $config['request_phone']);
          }
        }
      } catch (ApiErrorException $e) {
        // Server will return 500 to the frontend.
        $this->messenger->addError($e->getMessage());
        ErrorHelper::handleException($e);
      }
    }

    if ($intent->status == 'requires_confirmation' || $intent->status == 'requires_source_action' || $intent->status == 'requires_action') {
      # Tell the client to handle the action
      $response = [
        'requires_action' => TRUE,
        'payment_intent_client_secret' => $intent->client_secret,
        'payment_method_remote_id' => $values["data"]["payment_method_remote_id"],
        'redirect_uri' => Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])
            ->toString() . '/payment',
      ];
    } else {
      if ($intent->status == 'succeeded' || $intent->status == 'requires_capture') {
        # The payment didn’t need any additional actions and completed!
        # Handle post-payment fulfillment
        $response = [
          'success' => TRUE,
          'redirect_uri' => Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id(),])
              ->toString() . '/payment',
        ];
      } else {
        # Invalid status
        //http_response_code(500);
        $response = ['error' => 'Invalid PaymentIntent status'];
      }
    }
    return new JsonResponse($response);
  }

  /**
   * Check if the shipping profile exists or create a new one.
   * Afterwards return the profile for processing
   *
   * @param $order
   * @param $shipping_info
   * @param $requestPhone
   * @param $payerPhone
   *
   * @return mixed|null
   */
  private function getOrderShippingProfile($order, $shipping_info, $requestPhone, $payerPhone) {
    // We only get the full name from Apple/Google pay, so we need to "guess" the given
    // name and the family name.
    if (is_array($shipping_info)) {
      $shipping_info = json_decode(json_encode($shipping_info));
    }
    if (!isset($shipping_info->line1) && isset($shipping_info->addressLine[0])) {
      $shipping_info->line1 = $shipping_info->addressLine[0];
    }
    if (!isset($shipping_info->line2) && isset($shipping_info->addressLine[1])) {
      $shipping_info->line2 = $shipping_info->addressLine[1];
    }

    $names = [];

    if (isset($shipping_info->name)) {
      $names = explode(' ', $shipping_info->name);
    } else {
      $names = explode(' ', $shipping_info->recipient);
    }
    if (isset($shipping_info->postalCode)) {
      $shipping_info->postal_code = $shipping_info->postalCode;
    }
    if (isset($shipping_info->region)) {
      $shipping_info->state = $shipping_info->region;
    }

    $given_name = array_shift($names);
    $family_name = implode(' ', $names);
    $shippingProfile = NULL;

    //if the user is authenticated
    if ($order->getCustomerId() != 0) {
      //load all profiles to see if shipping address already exists to prevent database bloat
      $profiles = \Drupal::entityTypeManager()->getStorage('profile')
        ->loadByProperties([
          'uid' => $order->getCustomerId(),
        ]);
      foreach ($profiles as $profile) {
        //check if the address exists
        if (
          $profile->address->country_code == $shipping_info->country &&
          $profile->address->locality == $shipping_info->city &&
          $profile->address->postal_code == $shipping_info->postal_code &&
          $profile->address->address_line1 == $shipping_info->line1 &&
          $profile->address->given_name == $given_name &&
          $profile->address->family_name == $family_name
        ) {
          if ($profile->hasField('field_phone') && $requestPhone) {
            $profile->set('field_phone', $payerPhone);
          }
          $profile->save();
          $shippingProfile = $profile;
          break;
        }
      }
    }
    $orderShipments = $order->shipments->referencedEntities();

    $firstShipment = reset($orderShipments);

    //if profile exists set it as the order shipping profile else create new order
    if (!is_null($shippingProfile)) {
      $firstShipment->setShippingProfile($shippingProfile);
    } else {

      $shippingProfile = Profile::create([
        'type' => 'customer',
        'uid' => $order->getCustomerId(),
      ]);
      $shippingProfile->save();

      $shippingProfile->address->given_name = $given_name;
      $shippingProfile->address->family_name = $family_name;
      $shippingProfile->address->country_code = $shipping_info->country;
      $shippingProfile->address->locality = $shipping_info->city;
      $shippingProfile->address->administrative_area = $shipping_info->state;
      $shippingProfile->address->postal_code = $shipping_info->postal_code;
      $shippingProfile->address->address_line1 = $shipping_info->line1;
      if (isset($shipping_info->line2)) {
        $shippingProfile->address->address_line2 = $shipping_info->line2;
      }
      if ($shippingProfile->hasField('field_phone') && $requestPhone) {
        $shippingProfile->set('field_phone', $payerPhone);
      }
      $shippingProfile->save();
    }
    return $shippingProfile;
  }

  private function handleShipping($order, $shipping_info, $values, $requestPhone = FALSE) {
    $shipment = null;
    if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
      $shipment = $order->get('shipments')->entity;
    } else {
      $shipment = Shipment::create([
        'type' => 'default',
        'order_id' => $order->id(),
        'title' => 'Shipment',
        'state' => 'ready',
      ]);
    }

    // Loop through order items and add them to shipment
    foreach ($order->getItems() as $order_item) {
      $quantity = $order_item->getQuantity();
      $purchased_entity = $order_item->getPurchasedEntity();

      //TODO Skip weight if there is none, currently breaks order flow
      if ($purchased_entity->get('weight')->isEmpty()) {
        $weight = new Weight(1, WeightUnit::GRAM);
      } else {
        $weight_item = $purchased_entity->get('weight')->first();
        $weight = $weight_item->toMeasurement();
      }

      $shipment_item = new ShipmentItem([
        'order_item_id' => $order_item->id(),
        'title' => $purchased_entity->label(),
        'quantity' => $quantity,
        'weight' => $weight->multiply($quantity),
        'declared_value' => $order_item->getTotalPrice(),
      ]);
      $shipment->addItem($shipment_item);
    }

    $shipping_method = $order->get('shipments')->entity->getShippingMethod();

    if (!is_null($order->getData('payment_request_button_shipping'))) {
      $shipping_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');
      $shipping_method = $shipping_method_storage->load($order->getData('payment_request_button_shipping'));
    } elseif (isset($values['result']['shippingOption']) &&  $shipping_method->id() != $values['data']['result']['shippingOption']['id']) {
      $shipping_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');
      $shipping_method = $shipping_method_storage->load($values['data']['result']['shippingOption']['id']);
    }
    $shipment->setTitle($shipping_method->getName());
    $shipment->setShippingMethodId($shipping_method->id());
    $shipment->setShippingMethod($shipping_method);
    $shipment->save();
    $order->set('shipments', $shipment);

    $order->save();

    // Add proper shipping method
    $shipping_method_plugin = $shipping_method->getPlugin();
    $shipping_rates = $shipping_method_plugin->calculateRates($shipment);
    $shipping_rate = reset($shipping_rates);
    $shipping_service = $shipping_rate->getService()->getId();
    $shipping_method_amount = $shipping_rate->getAmount();

    //if the order currency is different from the default currency, check the if there is an correct shipping field set
    if (
      $shipping_method_amount->getCurrencyCode() != $order->getTotalPrice()
        ->getCurrencyCode()
      && isset($shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
          ->getCurrencyCode()])
    ) {
      $newAmount = $shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
        ->getCurrencyCode()];
      if (is_numeric($newAmount['number']) && $newAmount['number'] > 0) {
        $shipping_method_amount = new Price($newAmount['number'], $newAmount['currency_code']);
      }
    }
    //amount is not zero and there are not currency fields appended We use the commerce_currency_resolver to convert the amount
    elseif (
      $shipping_method_amount->getNumber() > 0 &&
      $shipping_method_amount->getCurrencyCode() != $order->getTotalPrice()
        ->getCurrencyCode()
      && isset($shipping_method_plugin->getConfiguration()['fields'][$order->getTotalPrice()
          ->getCurrencyCode()])
    ) {
      $shipping_method_amount = Drupal::service('commerce_currency_resolver.calculator')
        ->priceConversion($shipping_method_amount, $order->getTotalPrice()
          ->getCurrencyCode());
    }

    $shipment->setAmount($shipping_method_amount);

    $phone = '';
    if (is_array($values) && isset($values['data']['result']['shippingAddress']['phone'])) {
      $phone = $values['data']['result']['shippingAddress']['phone'];
    } elseif (!is_array($values)) { //just to check that it is an object
      $phone = $values->billing_details->phone;
    }
    $profile = $this->getOrderShippingProfile($order, $shipping_info, $requestPhone, $phone);

    $shipment->setShippingProfile($profile);
    $shipment->setShippingService($shipping_service);
    $shipment->save();

    $order->set('shipments', $shipment);
    $order->save();
    $var = 'tmp';
  }

  //check if order marked as paid
  //check if payment intent paid

  public function handleDecoupledOrder($commerceOrderUuId, $payment_intent) {
    $orders = $this->entityTypeManager->getStorage('commerce_order')
      ->loadByProperties(['uuid' => $commerceOrderUuId]);
    $order = reset($orders);


    //get the current currency order

    $currency = strtolower($order->getTotalPrice()->getCurrencyCode());

    $orderPaymentConfiguration = $this->getOrderPaymentConfiguration($currency);

    // Init Stripe and config
    $configName = $orderPaymentConfiguration->configName;
    $config = $orderPaymentConfiguration->config;

    Stripe::setApiKey($config['secret_key']);

    $paymentIntent = PaymentIntent::retrieve($payment_intent);

    if ($order->id() == $paymentIntent->metadata->order_id) {
      // If order has problems, take it back to cart
      if (empty($order) || $order->getState()->value == 'canceled') {
        $response = [
          'redirect_uri' => Url::fromRoute(Url::fromRoute('commerce_cart.page')
            ->toString()),
        ];
        return new JsonResponse($response);
      }

      $payments = \Drupal::entityQuery('commerce_payment')
        ->condition('order_id', $order->id())
        ->execute();


      $paymentCharge = $paymentIntent->charges['data'][0];
      $shipping_info = '';
      if (isset($paymentCharge->shipping->address)) { //if multiple shipping addresses are available
        $shipping_info = $paymentCharge->shipping->address;
      }
      if (count($payments)) {
        $payment = Payment::load(reset($payments)); //cannot flip
        $payment->setRemoteId($paymentIntent->id);
        $payment->save();
      } else {
        if ($order->hasItems()) {
          // Put order in proper status and assign values
          $this->setOrderData($order, $paymentIntent, $paymentIntent->metadata->email, $configName);

          // Create payment for this order and give it values
          $payment_gateway = $this->entityTypeManager
            ->getStorage('commerce_payment_gateway')
            ->load($configName);

          $current_user = $order->getCustomerId();

          $payment = $this->createDrupalPayment($order, $payment_gateway, $paymentIntent, $current_user);

          if ($config['capture_method'] == "manual") {
            $payment->setState("authorization");
          }
          $payment->save();
        }
      }

      // Deal with billing profile
      $profile = $order->getBillingProfile();

      if (empty($profile)) {
        $profile = Profile::create([
          'type' => 'customer',
          'uid' => $current_user,
        ]);
        $profile->save();
      }

      $current_name = $paymentCharge->billing_details->name;
      $names = explode(' ', $current_name);
      $given_name = array_shift($names);
      $family_name = implode(' ', $names);

      $profile->address->given_name = $given_name;
      $profile->address->family_name = $family_name;
      $profile->address->country_code = $paymentCharge->billing_details->address->country;
      $profile->address->locality = $paymentCharge->billing_details->address->city;
      $profile->address->administrative_area = $paymentCharge->billing_details->address->state;
      $profile->address->postal_code = $paymentCharge->billing_details->address->postal_code;
      $profile->address->address_line1 = $paymentCharge->billing_details->address->line1;
      if (isset($paymentCharge->billing_details->address->line2)) {
        $profile->address->address_line2 = $paymentCharge->billing_details->address->line2;
      }
      if ($profile->hasField('field_phone') && $config['request_phone']) {
        $profile->set('field_phone', $paymentCharge->billing_details->phone);
      }
      $profile->save();

      $order->setBillingProfile($profile);
      $order->save();

      // Deal with shipping stuff only if we selected that we want to fetch shipping address
      if ($config['request_shipping'] && !empty($shipping_info)) {
        $this->handleShipping($order, $shipping_info, $paymentCharge, $config['request_phone']);
      }

      if ($paymentIntent->status == PaymentIntent::STATUS_SUCCEEDED) {
        if (!$payment->isCompleted()) {
          $workflow = $payment->getState()->getWorkflow();
          $transition = $workflow->getTransition('capture');
          $payment->getState()->applyTransition($transition);
          $payment->save();
        }
        $response = [
          'success' => TRUE,
          'redirect_uri' => Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()])
              ->toString() . '/payment',
        ];
      }
    } else {
      //orders do not match
      $response = [
        'success' => FALSE,
        'error' => $this->t('Order id from the PaymentIntent does not match our records')
      ];
    }
    return new JsonResponse($response);
  }

  public function updateOrderShipping($commerceOrderUuId, $shippingId) {
    $orders = $this->entityTypeManager->getStorage('commerce_order')
      ->loadByProperties(['uuid' => $commerceOrderUuId]);
    $order = reset($orders);
    try {
      $order->setData('payment_request_button_shipping', $shippingId);
      $order->save();
      return new JsonResponse(['success' => true]);
    } catch (Exception $e) {
      $error_message = $e->getMessage();
      $this->messenger->addError($error_message, TRUE);
      Drupal::logger('commerce_stripe_payment_request_button')
        ->error(t('Failed to update order @order_id shippiping . @message', [
          '@order_id' => $order->id(),
          '@message' => $e,
        ]));
    }
    return new JsonResponse(['success' => false]);
  }

  /**
   * Checks access for the form page.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess() {
    $orderCollection = $this->entityTypeManager->getStorage('commerce_order')
      ->loadByProperties(['uuid' => $this->routeMatchInterface->getParameter('commerceOrderUuId')]);
    $order = reset($orderCollection);
    if (!$order || ($order && $order->getState()->getId() == 'canceled')) {
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    $access = AccessResult::allowedIf($order->hasItems())
      ->addCacheableDependency($order);
    return $access;
  }

  /**
   * Check if the payment is Google Pay or Apple pay
   *
   * @param PaymentIntent $paymentIntent
   * @return string $wallet
   */
  private function getWalletType(PaymentIntent $paymentIntent): string {
    $wallet = 'Google pay';
    if (isset($paymentIntent->charges->data[0])) {
      /**
       * @var \Stripe\Charge $charges
       */
      $charges = $paymentIntent->charges->data[0];
      $charges_values = $charges->jsonSerialize();
      $wallet = isset($charges_values['payment_method_details']['card']['wallet']['type']) ? 'Apple pay' : 'Google pay';
    }
    return $wallet;
  }

  /**
   * Get the order configuration by currency
   *
   * @param string $currency
   * @return class $data
   */
  private function getOrderPaymentConfiguration(string $currency) {

    $configBase = 'commerce_stripe_payment_request_button';
    $configCurrency = $configBase . '_' . $currency;
    $configName = $configCurrency;

    // Load curency config
    $config = \Drupal::config('commerce_payment.commerce_payment_gateway.' . $configCurrency);

    if (!$config->get('status')) {
      // Load default config
      $config = \Drupal::config('commerce_payment.commerce_payment_gateway.' . $configBase);
      $configName = $configBase;
    }

    $data = new \stdClass();

    $data->config = $config->get('configuration');
    $data->configName = $configName;

    return $data;
  }

  /**
   * Update drupal payment from the payment intent
   *
   * @param EntityInterface $order
   * @param string $intentId
   * @return PaymentIntent $paymentIntent
   */
  private function updateDrupalPaymentByPaymentIntent(EntityInterface $order, string $intentId) {

    $paymentIntent = PaymentIntent::retrieve($intentId);
    $payments = \Drupal::entityQuery('commerce_payment')
      ->condition('order_id', $order->id())
      ->execute();

    $payment = Payment::load(reset($payments));
    $payment->setRemoteId($paymentIntent->id);
    $payment->save();

    // Reset this step as it gets to NULL on cart refresh
    $order->get('checkout_flow')->setValue("payment_request_button_flow");
    $order->get('checkout_step')->setValue("payment");
    $order->save();
    return  $paymentIntent;
  }

  /**
   * Set the following order data: checkout_flow,checkout_step,email,payment_gateway,stripe_intent,wallet
   *
   * @param EntityInterface $order
   * @param PaymentIntent $paymentIntent
   * @param string $email
   * @param string $configName
   * @param string $shipping
   * @return void
   */
  private function setOrderData(EntityInterface $order, PaymentIntent $paymentIntent, string $email, string $configName, string $shipping = ''): void {
    $order->get('checkout_flow')->setValue("payment_request_button_flow");
    $order->get('checkout_step')->setValue("payment");
    $order->setEmail($email);
    $order->get('payment_gateway')->setValue($configName);
    $order->setData('stripe_intent', $paymentIntent->id);
    $order->setData('wallet', $this->getWalletType($paymentIntent));
    if (!empty($shipping)) {
      $order->setData('payment_request_button_shipping', $shipping);
    }
    $order->save();
  }

  /**
   * Create a drupal payment from the data provided
   *
   * @param EntityInterface $order
   * @param PaymentGateway $paymentGateway
   * @param PaymentIntent $paymentIntent
   * @param string $currentUser
   * @return Payment $payment
   */
  private function createDrupalPayment(EntityInterface $order, PaymentGateway $paymentGateway, PaymentIntent $paymentIntent, string $currentUser) {
    $payment = Payment::create([
      'state' => 'new',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $paymentGateway->id(),
      'order_id' => $order->id(),
      'remote_id' => $paymentIntent->id,
      'payment_gateway_mode' => $paymentGateway->getPlugin()->getMode(),
      'expires' => 0,
      'uid' => $currentUser,
    ]);
    $payment->save();
    return $payment;
  }
}
