<?php

namespace Drupal\commerce_stripe_payment_request_button\Element;

use Drupal\Core\Render\Element\RenderElement;

abstract class RequestButtonBase extends RenderElement {

  /**
   * Attaches the stripe payment request button library.
   *
   * @param array $element
   *   The element.
   *
   * @return array
   *   The render array.
   */
  public static function attachLibrary(array $element) {
    $element['#attached']['library'][] = 'commerce_stripe_payment_request_button/form';
    return $element;
  }

}
