<?php

namespace Drupal\commerce_stripe_payment_request_button\Element;

use Drupal\Core\Render\Markup;

/**
 * @RenderElement("stripe_request_button")
 */
class RequestButton extends RequestButtonBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#html_id' => 'StripeRequestButton',
      '#title' => t('Stripe request button makes shopping on our website even easier by using the payment information in your Apple pay, Google pay or Microsoft pay account'),
      '#order_id' => NULL,
      '#pre_render' => [
        [$class, 'preRender'],
        [$class, 'attachLibrary'],
      ],
    ];
  }

  public static function preRender($element) {
    if (empty($element['#order_id'])) {
      return $element;
    }

    $setup_button = '<div id="payment-request-button"></div>';
    $element['#markup'] = Markup::create('<div id="stripe-payment-request-setup-wrapper">' . $setup_button . '</div>');
    return $element;
  }

}
